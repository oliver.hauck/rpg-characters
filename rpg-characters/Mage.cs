﻿namespace rpg_characters
{
    public class Mage : Character
    {
        public Mage() : base(
            new PrimaryAttribute() { Strength = 1, Dexterity = 1, Intelligence = 8 },
            new PrimaryAttribute() { Strength = 1, Dexterity = 1, Intelligence = 5 },
            CharacterMainAttribute.Intelligence,
            new ArmourType[] { ArmourType.Cloth },
            new WeaponType[] { WeaponType.Wand, WeaponType.Staff })
        { }

    }
}
