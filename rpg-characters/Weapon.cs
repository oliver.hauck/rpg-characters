﻿using System.Linq;

namespace rpg_characters
{
    public class Weapon : Item
    {
        public WeaponType WeaponType { get; }
        public WeaponAttribute WeaponAttribute { get; }

        /// <summary>
        /// returns Damage per second (DPS) for this weapon
        /// (DamagePersecond (DPS) = BaseDamage * AttackSpeed)
        /// </summary>
        /// <returns>DamagePersecond (DPS)</returns>
        public double DPS
        {
            get
            {
                return WeaponAttribute.Damage * WeaponAttribute.AttackSpeed;
            }
        }

        public Weapon(string name, int level, Slot slot, WeaponType weaponType, int damage, double attackSpeed) :
            this(name, level, slot, weaponType, new WeaponAttribute
            {
                Damage = damage,
                AttackSpeed = attackSpeed
            }) { }
        public Weapon(string name, int level, Slot slot, WeaponType weaponType, WeaponAttribute weaponAttribute) : base (name, level, slot)
        {
            WeaponType = weaponType;
            WeaponAttribute = weaponAttribute;
        }

        /// <summary>
        /// checks whether this weapon can be equipped by character
        /// character is allowed to equip this weapon if
        /// character level is minimum the required level of this weapon and
        /// this weapon's type is member of AllowedWeaponTypes for the character
        /// </summary>
        /// <param name="character">instantiated character object</param>
        /// <exception cref="InvalidWeaponException">thrown if character is not allowed to euip this weapon</exception>
        public override void TryEquip(Character character)
        {
            try
            {
                base.TryEquip(character);
            }
            catch (InvalidItemException ex)
            {
                string message = $"The level of this {GetType().Name} [{RequiredLevel}] is too high for this {character.GetType().Name} of level {character.Level}";
                InvalidWeaponException iEx = new(message, ex, RequiredLevel, character.Level);
                throw iEx;
            }
            if (!character.AllowedWeaponTypes.Contains(WeaponType))
            {
                string message = $"{character.GetType().Name}s are not allowed to euip weapons of type {WeaponType}";
                InvalidWeaponException iEx = new(message, null, character.AllowedWeaponTypes);
                throw iEx;
            }
        }
    }
}
