﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace rpg_characters
{
    public abstract class Character
    {
        private PrimaryAttribute baseAttribute;

        public string Name { get; set; }
        public int Level { get; set; }
        public PrimaryAttribute BaseAttribute => baseAttribute;
        public PrimaryAttribute LevelUpAttribute { get; }
        public CharacterMainAttribute MainAttributeMember { get; }
        public PrimaryAttribute TotalAttribute => GetTotalAttribute();
        public double Damage => GetDamage();
        public ArmourType[] AllowedArmourTypes { get; }
        public WeaponType[] AllowedWeaponTypes { get; }
        public Dictionary<Slot, Item> Equipment { get; }
        public Character(
            PrimaryAttribute baseAttribute, 
            PrimaryAttribute levelUpAttribute,
            CharacterMainAttribute mainAttributeMember,
            ArmourType[] allowedArmourTypes,
            WeaponType[] allowedWepaonTypes)
        {
            this.baseAttribute = baseAttribute;
            LevelUpAttribute = levelUpAttribute;
            MainAttributeMember = mainAttributeMember;
            AllowedArmourTypes = allowedArmourTypes;
            AllowedWeaponTypes = allowedWepaonTypes;
            Equipment = new Dictionary<Slot, Item>();
            Level = 1;
        }

        /// <summary>
        /// raises the level of the character by numberOfLevels
        /// adds numberOfLevels times the levelUpAttribute to the characters current BaseAttribute
        /// </summary>
        /// <param name="numberOfLevels">number of Levels the current character's level should be raised</param>
        public void LevelUp(int numberOfLevels = 1)
        {
            baseAttribute += numberOfLevels * LevelUpAttribute;
            Level += numberOfLevels;
        }

        /// <summary>
        /// outputs characters statistics in a string
        /// </summary>
        /// <returns>characters statistics</returns>
        public string DisplayStats()
        {
            string formatString = "{0,15}:{1,10}";
            StringBuilder stringBuilder = new();
            stringBuilder.AppendFormat(formatString, "Name", Name).AppendLine();
            stringBuilder.AppendFormat(formatString, "Level", Level).AppendLine();
            stringBuilder.AppendFormat(formatString, "Strength", TotalAttribute.Strength).AppendLine();
            stringBuilder.AppendFormat(formatString, "Dexterity", TotalAttribute.Dexterity).AppendLine();
            stringBuilder.AppendFormat(formatString, "Intellgence", TotalAttribute.Intelligence).AppendLine();
            stringBuilder.AppendFormat(formatString, "Damage", Damage).AppendLine();
            return stringBuilder.ToString();
        }

        /// <summary>
        /// adds / replaces an item to / in the character's equipment
        /// </summary>
        /// <param name="item">item that should be added to the characters equipment</param>
        /// <exception cref="InvalidWeaponException">occurs if character type is not allowed to equip this weapon
        /// or if weapon level is higher than the character's level</exception>
        /// <exception cref="InvalidArmourException">occurs if character type is not allowed to equip this armour
        /// or if armour level is higher than the character's level</exception>
        public string Equip(Item item)
        {
            item.TryEquip(this);
            Equipment[item.Slot] = item;
            return $"New {item.GetType().Name.ToLower()} equipped!";
        }

        /// <summary>
        /// removes an Item from the characters equipment
        /// </summary>
        /// <param name="slot">the slot from which the current item should be removed</param>
        public string Disrobe(Slot slot)
        {
            string message = $"Slot {slot} is already empty...";
            if (Equipment.Keys.Contains(slot))
            {
                Item item2Disrobe = Equipment[slot];
                string itemName = item2Disrobe.ItemName;
                Equipment.Remove(slot);
                message = $"{itemName} removed from slot { slot }.";
            }
            return message;
        }

        /// <summary>
        /// calculates the total attributes of the character
        /// the total attributes are the attributes from the character's level +
        /// the attributes from all equipped armor
        /// </summary>
        /// <returns>total attributes</returns>
        private PrimaryAttribute GetTotalAttribute()
        {
            PrimaryAttribute totalAttributes = BaseAttribute;
            //--- Add PrimaryAttributes from all equipped armor to BaseAttribute
            if (null != Equipment)
            {
                foreach (Armour myArmour in Equipment.Values.OfType<Armour>())
                {
                    totalAttributes += myArmour.ArmourAttribute;
                }
            }
            return totalAttributes;
        }

        /// <summary>
        /// calculates the damage the character can deal
        /// based on Character's total attributes and euipped weapon
        /// characterDamage = WeaponDPS * (1 + totalAttribute / 100)
        /// If the character has no weapon equipped, their Damage equals 1.
        /// </summary>
        /// <returns>damage the character can deal</returns>
        private double GetDamage()
        {
            double damage;
            double myDPS;
            if (HasWeapon())
            {
                myDPS = ((Weapon)Equipment[Slot.Weapon]).DPS;
            }
            else
            {
                myDPS = 1;
            }

            var myAttributeValue = MainAttributeMember switch
            {
                CharacterMainAttribute.Strength => TotalAttribute.Strength,
                CharacterMainAttribute.Dexterity => TotalAttribute.Dexterity,
                CharacterMainAttribute.Intelligence => TotalAttribute.Intelligence,
                _ => throw new NotImplementedException($"The main attribute member [{MainAttributeMember}] of class [{GetType().Name}] is not implemented."),
            };
            damage = myDPS * (1 + (double)myAttributeValue / 100);

            return damage;
        }

        /// <summary>
        /// checks whether character is equipped with a weapon
        /// </summary>
        /// <returns></returns>
        private bool HasWeapon()
        {
            if (null != Equipment)
            {
                return Equipment.Select(item => item.Value).OfType<Weapon>().Any();
            }
            else return false;
        }
    }


}
