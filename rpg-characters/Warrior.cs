﻿namespace rpg_characters
{
    public class Warrior : Character
    {
        public Warrior() : base(
            new PrimaryAttribute() { Strength = 5, Dexterity = 2, Intelligence = 1 },
            new PrimaryAttribute() { Strength = 3, Dexterity = 2, Intelligence = 1 },
            CharacterMainAttribute.Strength,
            new ArmourType[] { ArmourType.Mail, ArmourType.Plate },
            new WeaponType[] { WeaponType.Axe, WeaponType.Hammer, WeaponType.Sword })
        { }
    }
}
