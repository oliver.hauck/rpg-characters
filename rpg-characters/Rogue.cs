﻿namespace rpg_characters
{
    public class Rogue : Character
    {
        public Rogue() : base(
            new PrimaryAttribute() { Strength = 2, Dexterity = 6, Intelligence = 1 },
            new PrimaryAttribute() { Strength = 1, Dexterity = 4, Intelligence = 1 },
            CharacterMainAttribute.Dexterity,
            new ArmourType[] { ArmourType.Leather, ArmourType.Mail },
            new WeaponType[] { WeaponType.Dagger, WeaponType.Sword })
        { }
    }
}
