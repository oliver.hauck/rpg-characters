﻿namespace rpg_characters
{
    public struct PrimaryAttribute
    {
        public int Strength { get; set; }
        public int Dexterity { get; set; }
        public int Intelligence { get; set; }
        public static PrimaryAttribute operator + (PrimaryAttribute Attribute1, PrimaryAttribute Attribute2)
        {
            PrimaryAttribute primaryAttribute = Attribute1;
            primaryAttribute.Strength += Attribute2.Strength;
            primaryAttribute.Dexterity += Attribute2.Dexterity;
            primaryAttribute.Intelligence += Attribute2.Intelligence;
            return primaryAttribute;
        }
        public static PrimaryAttribute operator *(PrimaryAttribute Attribute1, int factor)
        {
            PrimaryAttribute primaryAttribute = Attribute1;
            primaryAttribute.Strength *= factor;
            primaryAttribute.Dexterity *= factor;
            primaryAttribute.Intelligence *= factor;
            return primaryAttribute;
        }
        public static PrimaryAttribute operator *(int factor, PrimaryAttribute Attribute1)
        {
            PrimaryAttribute primaryAttribute = Attribute1;
            primaryAttribute.Strength *= factor;
            primaryAttribute.Dexterity *= factor;
            primaryAttribute.Intelligence *= factor;
            return primaryAttribute;
        }
        public override string ToString()
        {
            return $"PrimaryAttribute {{Strength: {Strength}, Dexterity: {Dexterity}, Intelligence: {Intelligence} }}";
        }
    }
    public struct WeaponAttribute
    {
        public int Damage { get; set; }
        public double AttackSpeed { get; set; }
    }
}
