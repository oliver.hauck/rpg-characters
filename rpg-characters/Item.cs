﻿namespace rpg_characters
{
    public abstract class Item
    {
        public Item(string itemName, int requiredLevel, Slot slot)
        {
            ItemName = itemName;
            RequiredLevel = requiredLevel;
            Slot = slot;
        }
        public string ItemName { get; }
        public int RequiredLevel { get; }
        public Slot Slot { get; }

        /// <summary>
        /// checks whether character can equip item based on their level and
        /// throws exception if level of the character is insufficient to equip this item
        /// </summary>
        /// <param name="character">instantiated character object that is supposed to euip the item</param>
        /// <exception cref="InvalidItemException">thrown if this item is leveled too high for character</exception>
        public virtual void TryEquip(Character character)
        {
            if (RequiredLevel > character.Level)
            {
                string message = $"The level of this {GetType().Name} [{RequiredLevel}] is too high for this {character.GetType().Name} of level {character.Level}";
                InvalidItemException ex = new(message, null, RequiredLevel, character.Level);
                throw ex;
            }
        }

    }
}
