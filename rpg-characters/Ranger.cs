﻿namespace rpg_characters
{
    public class Ranger : Character
    {
        public Ranger() : base(
            new PrimaryAttribute() { Strength = 1, Dexterity = 7, Intelligence = 1 },
            new PrimaryAttribute() { Strength = 1, Dexterity = 5, Intelligence = 1 },
            CharacterMainAttribute.Dexterity,
            new ArmourType[] { ArmourType.Leather, ArmourType.Mail },
            new WeaponType[] { WeaponType.Bow })
        { }
    }
}
