﻿using System;

namespace rpg_characters
{
    [Flags]
    public enum Slot
    {
        Head,
        Body,
        Legs,
        Weapon
    }
    public enum WeaponType
    {
        Axe,
        Bow,
        Dagger,
        Hammer,
        Staff,
        Sword,
        Wand
    }
    public enum ArmourType
    {
        Cloth,
        Leather,
        Mail,
        Plate
    }

    public enum CharacterMainAttribute
    {
        Strength,
        Dexterity,
        Intelligence
    }
}
