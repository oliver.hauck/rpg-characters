﻿using System.Linq;

namespace rpg_characters
{
    public class Armour : Item
    {
        public Armour(string itemName, int requiredLevel, Slot slot, ArmourType armourType, int Strength, int Dexterity, int Intelligence) : 
            this(itemName, requiredLevel, slot, armourType, new() { Strength = Strength, Dexterity = Dexterity, Intelligence = Intelligence }) { }
        public Armour(string itemName, int requiredLevel, Slot slot, ArmourType armourType, PrimaryAttribute armourAttribute) : base(itemName, requiredLevel, slot)
        {
            ArmourType = armourType;
            ArmourAttribute = armourAttribute;
        }
        public ArmourType ArmourType { get; }
        public PrimaryAttribute ArmourAttribute { get; }

        /// <summary>
        /// checks whether this armour can be equipped by character
        /// character is allowed to equip this armour if
        /// character level is minimum the required level of this armour and
        /// this armour's type is member of AllowedArmourTypes for the character
        /// </summary>
        /// <param name="character">instantiated character object</param>
        /// <exception cref="InvalidArmourException">thrown if character is not allowed to euip this armour</exception>
        public override void TryEquip(Character character)
        {
            try
            {
                base.TryEquip(character);
            }
            catch (InvalidItemException ex)
            {
                string message = $"The level of this {GetType().Name} [{RequiredLevel}] is too high for this {character.GetType().Name} of level {character.Level}";
                InvalidArmourException iEx = new(message, ex, RequiredLevel, character.Level);
                throw iEx;
            }
            if(!character.AllowedArmourTypes.Contains(ArmourType))
            {
                string message = $"{character.GetType().Name}s are not allowed to euip armour of type {ArmourType}";
                InvalidArmourException iEx = new(message, null, character.AllowedArmourTypes);
                throw iEx;
            }
        }
    }
}
