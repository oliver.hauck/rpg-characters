﻿using System;

namespace rpg_characters
{

    [Serializable]
    public class InvalidItemException : Exception
    {
        public InvalidItemException() { }
        public InvalidItemException(string message) : base(message) { }
        public InvalidItemException(string message, Exception inner) : base(message, inner) { }
        public InvalidItemException(string message, Exception inner, int itemLevel, int characterLevel) : base(message, inner)
        {
            Data.Add("Date", DateTime.Now);
            Data.Add("Minimum required Character Level", itemLevel);
            Data.Add("actual Character Level", characterLevel);
        }
        protected InvalidItemException(
          System.Runtime.Serialization.SerializationInfo info,
          System.Runtime.Serialization.StreamingContext context) : base(info, context) { }
    }


    [Serializable]
    public class InvalidWeaponException : InvalidItemException
    {
        public InvalidWeaponException() : base() { }
        public InvalidWeaponException(string message) : base(message) { }
        public InvalidWeaponException(string message, Exception inner) : base(message, inner) { }
        public InvalidWeaponException(string message, Exception inner, WeaponType[] allowedWeaponTypes) : base(message, inner) {
            Data.Add("Date", DateTime.Now);
            if(null != allowedWeaponTypes)
            {
                Data.Add("AllowedWeaponTypes", allowedWeaponTypes);
            }
        }
        public InvalidWeaponException(string message, Exception inner, int itemLevel, int characterLevel) : base(message, inner, itemLevel, characterLevel) { }
        protected InvalidWeaponException(
          System.Runtime.Serialization.SerializationInfo info,
          System.Runtime.Serialization.StreamingContext context) : base(info, context) { }
    }

    [Serializable]
    public class InvalidArmourException : InvalidItemException
    {
        public InvalidArmourException() : base() { }
        public InvalidArmourException(string message) : base(message) { }
        public InvalidArmourException(string message, Exception inner) : base(message, inner) { }
        public InvalidArmourException(string message, Exception inner, ArmourType[] allowedArmourTypes) : base(message, inner)
        {
            Data.Add("Date", DateTime.Now);
            if (null != allowedArmourTypes)
            {
                Data.Add("AllowedArmourTypes", allowedArmourTypes);
            }
        }
        public InvalidArmourException(string message, Exception inner, int itemLevel, int characterLevel) : base(message, inner, itemLevel, characterLevel) { }
        protected InvalidArmourException(
          System.Runtime.Serialization.SerializationInfo info,
          System.Runtime.Serialization.StreamingContext context) : base(info, context) { }
    }
}
