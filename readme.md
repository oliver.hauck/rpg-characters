# RPG Characters

This is a very very simple .NET CORE 5 console application that is solely built for educational
purposes.

In the game there are currently four classes that a character/hero can be:
- Mage
- Ranger
- Rogue
- Warrior

Characters in the game have several types of attributes, which represent different aspects of the character.

# Install

[.NET CORE 5 SDK](https://dotnet.microsoft.com/en-us/download/dotnet/5.0)
 
[Visual Studio Code](https://code.visualstudio.com/download) (recommended)


# Usage
`dotnet test`

Please visit the [diagram](./OH_RPGCharactersClassDesign.pdf) of the various classes and their interactions to help visualize the application and its
functionality.

# Maintainers
<https://gitlab.com/oliver.hauck> 

# Contributing
This project is not supposed to have any contributors.
It is supposed to grade the individual who composed it.
Right after that graduation it will be - most likely - instantly discontinued...

# License
[Apache License &copy; 2022](./LICENSE)
