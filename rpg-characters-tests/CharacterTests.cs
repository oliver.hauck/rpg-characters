using System;
using Xunit;
using rpg_characters;

namespace rpg_characters_tests
{
    public class CharacterTests
    {
        /// <summary>
        /// checks whether character's level equals 1 after instantiation
        /// </summary>
        [Fact]
        public void Ctor_NewCharacterIsCreated_LevelEqualsOne()
        {
            //--- arrange
            int expected = 1;
            Character character = new Mage();

            //--- act
            int actual = character.Level;

            //--- assert
            Assert.Equal(expected, actual);
        }

        /// <summary>
        /// checks whether level is raised to 2 after character leveled up for the first time
        /// </summary>
        [Fact]
        public void LevelUp_FirstTimeCharacterGainsLevel_LevelEquals2()
        {
            //--- arrange
            int expected = 2;
            Character character = new Warrior();

            //--- act
            character.LevelUp(1);
            int actual = character.Level;

            //--- assert
            Assert.Equal(expected, actual);
        }

        /// <summary>
        /// This test checks whether each character type has the correct Attributes (Strength, Dexterity, Intelligence) set 
        /// after it has been created (is still at current level 1)
        /// </summary>
        /// <param name="characterType">type of the character (Mage, Ranger, Rogue or Warrior</param>
        /// <param name="strength">expected Strength of the character at level 1</param>
        /// <param name="dexterity">expected Dexterity of the character at level 1</param>
        /// <param name="intelligence">expected Intelligence of the character at level 1</param>
        [Theory]
        [InlineData(typeof(Mage), 1, 1, 8)]
        [InlineData(typeof(Ranger), 1, 7, 1)]
        [InlineData(typeof(Rogue), 2, 6, 1)]
        [InlineData(typeof(Warrior), 5, 2, 1)]
        public void Ctor_CharacterIsCreated_CharacterHasLevelOneStats(Type characterType, int strength, int dexterity, int intelligence)
        {
            //--- arrange
            PrimaryAttribute expected = new() { Strength = strength, Dexterity = dexterity, Intelligence = intelligence };
            //--- act
            Character character = (Character)Activator.CreateInstance(characterType);
            PrimaryAttribute actual = character.BaseAttribute;
            //--- assert
            Assert.Equal(expected, actual);
        }

        /// <summary>
        /// This test checks whether each character type has the correct Attributes (Strength, Dexterity, Intelligence) set 
        /// after it has leveled up for the first time (is now at current level 2
        /// </summary>
        /// <param name="characterType">type of the character (Mage, Ranger, Rogue or Warrior</param>
        /// <param name="strength">expected Strength of the character at level 2</param>
        /// <param name="dexterity">expected Dexterity of the character at level 2</param>
        /// <param name="intelligence">expected Intelligence of the character at level 2</param>
        [Theory]
        [InlineData(typeof(Mage), 2, 2, 13)]
        [InlineData(typeof(Ranger), 2, 12, 2)]
        [InlineData(typeof(Rogue), 3, 10, 2)]
        [InlineData(typeof(Warrior), 8, 4, 2)]
        public void LevelUp_CharacterIsLeveledUpOneLevel_CharacterHasLevelTwoStats(Type characterType, int strength, int dexterity, int intelligence)
        {
            //--- arrange
            PrimaryAttribute expected = new() { Strength = strength, Dexterity = dexterity, Intelligence = intelligence };
            Character character = (Character)Activator.CreateInstance(characterType);
            //--- act
            character.LevelUp(1);
            PrimaryAttribute actual = character.BaseAttribute;
            //--- assert
            Assert.Equal(expected, actual);
        }
    }
}
