﻿using Xunit;
using rpg_characters;

namespace rpg_characters_tests
{
    public class ItemTests
    {
        /// <summary>
        /// If a character tries to equip a high level weapon, InvalidWeaponException should be thrown.
        /// </summary>
        [Fact]
        public void Equip_CharacterTriesToEquipTooHighLeveledWeapon_ShouldThrowInvalidWeaponException()
        {
            //--- arrange
            Weapon testAxe = new("Advannced Axe", 2, Slot.Weapon, WeaponType.Axe, 8, 1.1);
            Warrior testWarrior = new();
            //--- act & assert
            Assert.Throws<InvalidWeaponException>(() => testWarrior.Equip(testAxe));
        }
        /// <summary>
        /// If a character tries to equip a high level armor piece, InvalidArmorException should be thrown.
        /// </summary>
        [Fact]
        public void Equip_CharacterTriesToEquipTooHighLeveledArmour_ShouldThrowInvalidArmourException()
        {
            //--- arrange
            Armour testPlateBody = new("Advanced plate body armor", 2, Slot.Body, ArmourType.Plate, 3, 0, 0);
            Warrior testWarrior = new();
            //--- act & assert
            Assert.Throws<InvalidArmourException>(() => testWarrior.Equip(testPlateBody));
        }
        /// <summary>
        /// If a character tries to equip the wrong weapon type, InvalidWeaponException should be thrown.
        /// </summary>
        [Fact]
        public void Equip_CharacterTriesToEquipWrongWeaponType_ShouldThrowInvalidWeaponException()
        {
            //--- arrange
            Weapon testBow = new("Common bow", 1, Slot.Weapon, WeaponType.Bow, 12, 0.8);
            Warrior testWarrior = new();
            //--- act & assert
            Assert.Throws<InvalidWeaponException>(() => testWarrior.Equip(testBow));
        }
        /// <summary>
        /// If a character tries to equip the wrong armor type, InvalidArmorException should be thrown.
        /// </summary>
        [Fact]
        public void Equip_CharacterTriesToEquipWrongArmourType_ShouldThrowInvalidArmourException()
        {
            //--- arrange
            Armour testClothHead = new("Common cloth head armor", 1, Slot.Head, ArmourType.Cloth, 0, 0, 5);
            Warrior testWarrior = new();
            //--- act & assert
            Assert.Throws<InvalidArmourException>(() => testWarrior.Equip(testClothHead));
        }
        /// <summary>
        /// If a character equips a valid weapon, a success message should be returned.
        /// </summary>
        [Fact]
        public void Equip_CharacterTriesToEquipValidWeapon_ShouldReturnSuccessMessage()
        {
            //--- arrange
            Weapon testAxe = new("Common Axe", 1, Slot.Weapon, WeaponType.Axe, 7, 1.1);
            Warrior testWarrior = new();
            string expected = "New weapon equipped!";
            //--- act
            string actual = testWarrior.Equip(testAxe);
            //--- assert
            Assert.Equal(expected, actual);
        }
        /// <summary>
        /// If a character equips a valid armor piece, a success message should be returned.
        /// </summary>
        [Fact]
        public void Equip_CharacterTriesToEquipValidArmour_ShouldReturnSuccessMessage()
        {
            //--- arrange
            Armour testPlateBody = new("Common plate body armour", 1, Slot.Body, ArmourType.Plate, 1, 0, 0);
            Warrior testWarrior = new();
            string expected = "New armour equipped!";
            //--- act
            string actual = testWarrior.Equip(testPlateBody);
            //--- assert
            Assert.Equal(expected, actual);
        }

        /// <summary>
        /// If a character tries to disrobe an unequipped / empty slot, an accordant informational message should be returned.
        /// </summary>
        [Fact]
        public void Disrobe_CharacterTriesToDisrobeArmourFromUnequippedAlot_ShouldReturnEmptySlotMessage()
        {
            //--- arrange
            Warrior testWarrior = new();
            string expected = "Slot Body is already empty...";
            //--- act
            string actual = testWarrior.Disrobe(Slot.Body);
            //--- assert
            Assert.Equal(expected, actual);
        }

        /// <summary>
        /// If a character disrobes an equipped armour piece, a success message should be returned.
        /// </summary>
        [Fact]
        public void Disrobe_CharacterDisrobesArmour_ShouldReturnSuccessMessage()
        {
            //--- arrange
            Armour testPlateBody = new("Common plate body armour", 1, Slot.Body, ArmourType.Plate, 1, 0, 0);
            Warrior testWarrior = new();
            testWarrior.Equip(testPlateBody);
            string expected = "Common plate body armour removed from slot Body." ;
            //--- act

            string actual = testWarrior.Disrobe(Slot.Body);
            //--- assert
            Assert.Equal(expected, actual);
        }

        /// <summary>
        /// Calculate Damage if no weapon is equipped.
        /// </summary>
        [Fact]
        public void Damage_CharacterWithoutWeapon_ShouldReturnCorrectDamage()
        {
            //--- arrange
            Warrior testWarrior = new();
            double expected = 1 * (1 + (double)5 / 100);
            //--- act
            double actual = testWarrior.Damage;
            //--- assert
            Assert.Equal(expected, actual);
        }
        /// <summary>
        /// Calculate Damage with valid weapon equipped.
        /// </summary>
        [Fact]
        public void Damage_CharacterEquippedWithWeapon_ShouldReturnCorrectDamage()
        {
            //--- arrange
            Weapon testAxe = new("Common Axe", 1, Slot.Weapon, WeaponType.Axe, 7, 1.1);
            Armour testPlateBody = new("Common plate body armour", 1, Slot.Body, ArmourType.Plate, 1, 0, 0);
            Warrior testWarrior = new();
            testWarrior.Equip(testAxe);
            testWarrior.Equip(testPlateBody);
            double expected = (7 * 1.1) * (1 + (double)(5 + 1) / 100);
            //--- act
            double actual = testWarrior.Damage;
            //--- assert
            Assert.Equal(expected, actual);
        }
        /// <summary>
        /// Calculate Damage with valid weapon and armor equipped.
        /// </summary>
        [Fact]
        public void Damage_CharacterEquippedWithWeaponAndArmour_ShouldReturnCorrectDamage()
        {
            //--- arrange
            Weapon testAxe = new("Common Axe", 1, Slot.Weapon, WeaponType.Axe, 7, 1.1);
            Warrior testWarrior = new();
            testWarrior.Equip(testAxe);
            double expected = (7 * 1.1) * (1 + (double)5 / 100);
            //--- act
            double actual = testWarrior.Damage;
            //--- assert
            Assert.Equal(expected, actual);
        }
    }
}
